## Xin cảm ơn bạn đã dành thời gian quý báu để cống hiến <3

**Hãy đảm bảo bạn đã kiểm tra những điều sau và tích vào những điều đã làm:**

- [ ] File mdx của bạn chỉ có 1 thẻ H1
- [ ] Các nội dung chính được chia theo level từ H1 > H2 > H3 ..etc...

Mỗi bài đều có:

- [ ] Giải thích về lý thuyết, ý tưởng
- [ ] Có sử dụng ảnh
- [ ] Giải thích cách hoạt động của code(bao gồm cả việc comment trong code)
- [ ] Nhúng code mẫu vào bài để học sinh dễ copy
- [ ] Bài tập đơn giản để áp dụng kiến thức
- [ ] Giải bài tập kèm lời giải thích
- [ ] Noted lại các khái niệm bằng tiếng việt ngắn gọn dễ hiểu
- [ ] Tự kiểm tra lại tất cả nội dung và chụp hình phần bạn làm vào merge request
- [ ] Hỏi 2 người review bài của bạn và nhờ họ nhận xét trực tiếp vào code
- [ ] Hỏi A Huy merge code
